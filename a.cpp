#include <iostream>
#include <algorithm>
#include <iostream>

int main() 
{
	std::cout << "Hello, World!";

	std::vector<int> nums{3, 4, 2, 8, 15, 267};

	auto print = [](const int& n) { std::cout << " " << n; };

	std::cout << "before:";
	std::for_each(nums.begin(), nums.end(), print);
	std::cout << '\n';

	return 0;
}
